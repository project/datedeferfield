<?php

namespace Drupal\datedeferfield\Plugin\Field\FieldFormatter;

use Drupal\Component\Datetime\DateTimePlus;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\datedeferfield\Plugin\Field\FieldType\DateDeferFieldItem;

/**
 * Plugin implementation of the 'datedeferfield_default' formatter.
 *
 * @FieldFormatter(
 *   id = "datedeferfield_diff",
 *   label = @Translation("Diff"),
 *   field_types = {
 *     "datedeferfield"
 *   }
 * )
 */
class DateDeferFieldDiffFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $now = DateTimePlus::createFromTimestamp(\Drupal::time()->getRequestTime());
    $key = $this->getSetting('component');
    $signFilter = $this->getSetting('sign_filter');
    $elements = [];

    foreach ($items as $delta => $item) {
      /** @var \Drupal\Core\Datetime\DrupalDateTime $date */
      $date = $item->{"date_$key"};
      if (isset($date)) {
        // The diff is positive if that date has passed.
        $diff = $date->diff($now);
        if ($diff->invert) {
          $filter = in_array('negative', $signFilter);
        }
        else {
          $filter = in_array('non_negative', $signFilter);
        }
        if ($filter) {
          $elements[$delta]['date']['#markup'] = $diff->format('%r%a');
        }
      }
      if (!empty($item->_attributes)) {
        $elements[$delta]['#attributes'] += $item->_attributes;
        // Unset field item attributes since they have been included in the
        // formatter output and should not be rendered in the field template.
        unset($item->_attributes);
      }
    }
    // As we format relative dates, this expires midnight, neglecting timezones.
    // @todo Improve.
    $maxAge = (new DateTimePlus('tomorrow'))->getTimestamp() - $now->getTimestamp();
    $elements['#cache']['max-age'] = $maxAge;

    return $elements;
  }

  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);
    $form['component'] = [
      '#type' => 'select',
      '#title' => $this->t('Component'),
      '#options' => DateDeferFieldItem::components(),
      '#default_value' => $this->getSetting('component'),
    ];
    $form['sign_filter'] = [
      '#type' => 'select',
      '#title' => $this->t('Filter by'),
      '#multiple' => TRUE,
      '#options' => $this->signFilterOptions(),
      '#default_value' => $this->getSetting('sign_filter'),
    ];

    return $form;
  }

  public function settingsSummary() {
    $summary = parent::settingsSummary();
    if ($component = $this->getSetting('component')) {
      $summary[] = $this->t('Component: @component', ['@component' => DateDeferFieldItem::components()[$component]]);
    }
    if ($signFilter = $this->getSetting('sign_filter')) {
      $signFilterMarkup = implode(', ', array_map(function ($key) {
        return $this->signFilterOptions()[$key];
      }, $signFilter));
      $summary[] = $this->t('Filter by: @sign_filter', ['@sign_filter' => $signFilterMarkup]);
    }
    return $summary;
  }

  public static function defaultSettings() {
    $settings = parent::defaultSettings() + [
      'component' => '',
      'sign_filter' => '',
    ];
    return $settings;
  }

  public function signFilterOptions() {
    return [
      'negative' => $this->t('Negative'),
      'non_negative' => $this->t('Non-negative'),
    ];
  }

}
