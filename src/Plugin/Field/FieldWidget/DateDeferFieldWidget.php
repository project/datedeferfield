<?php

namespace Drupal\datedeferfield\Plugin\Field\FieldWidget;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\datedeferfield\Plugin\Field\FieldType\DateDeferFieldItem;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\datetime\Plugin\Field\FieldWidget\DateTimeWidgetBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'datedeferfield_default' widget.
 *
 * @FieldWidget(
 *   id = "datedeferfield_default",
 *   label = @Translation("Date defer"),
 *   field_types = {
 *     "datedeferfield"
 *   }
 * )
 */
class DateDeferFieldWidget extends DateTimeWidgetBase {

  /**
   * The date format storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $dateStorage;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, EntityStorageInterface $date_storage) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);

    $this->dateStorage = $date_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager')->getStorage('date_format')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $singleElement = parent::formElement($items, $delta, $element, $form, $form_state)['value'];

    $date_format = $this->dateStorage->load('html_date')->getPattern();

    $element['#theme_wrappers'][] = 'fieldset';
    $element['#element_validate'][] = [$this, 'validateDates'];
    foreach (DateDeferFieldItem::components() as $key => $label) {
      $element["value_$key"] = [
        '#title' => $label,
        '#date_timezone' => DateTimeItemInterface::STORAGE_TIMEZONE,
        '#date_date_format' => $date_format,
        '#date_date_element' => 'date',
        '#date_date_callbacks' => [],
        '#date_time_format' => '',
        '#date_time_element' => 'none',
        '#date_time_callbacks' => [],
      ] + $singleElement;
      /** @var \Drupal\Core\Datetime\DrupalDateTime $date */
      if ($date = $items[$delta]->{"date_$key"}) {
        $element["value_$key"]['#default_value'] = $this->createDefaultValue($date, $element["value_$key"]['#date_timezone']);
      }
    }

    return $element;
  }

  /**
   * {@inheritDoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    // The widget form element type has transformed the value to a
    // DrupalDateTime object at this point. We need to convert it back to the
    // storage timezone and format.
    $storage_format = DateTimeItemInterface::DATE_STORAGE_FORMAT;
    $storage_timezone = new \DateTimezone(DateTimeItemInterface::STORAGE_TIMEZONE);
    foreach ($values as &$item) {
      foreach (DateDeferFieldItem::components() as $key => $label) {
        if (($date = $item["value_$key"] ?? NULL) && $date instanceof DrupalDateTime) {
          $item["value_$key"] = $date->setTimezone($storage_timezone)->format($storage_format);
        }
      }
    }
    return $values;
  }

  /**
   * #element_validate callback to ensure that the start date <= the end date.
   *
   * @param array $element
   *   An associative array containing the properties and children of the
   *   generic form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   */
  public function validateDates(array &$element, FormStateInterface $form_state, array &$complete_form) {
    $components = DateDeferFieldItem::components();
    $componentKeys = array_keys($components);
    foreach ($componentKeys as $i1 => $key1) {
      foreach ($componentKeys as $i2 => $key2) {
        if ($i1 < $i2) {
          $date1 = $element["value_$key1"]['#value']['object'];
          $date2 = $element["value_$key2"]['#value']['object'];
          if ($date1 instanceof DrupalDateTime && $date2 instanceof DrupalDateTime) {
            if ($date1->getTimestamp() !== $date2->getTimestamp()) {
              $interval = $date1->diff($date2);
              if ($interval->invert === 1) {
                $form_state->setError($element, $this->t('The @title %component2 date cannot be before the %component1 date',
                  [
                    '@title' => $element['#title'],
                    '%component1' => $components[$key1],
                    '%component2' => $components[$key2]
                  ]));
              }
            }
          }
        }
      }
    }
  }

}
