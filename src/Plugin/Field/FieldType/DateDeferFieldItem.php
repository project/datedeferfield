<?php

namespace Drupal\datedeferfield\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\datedeferfield\DateComputed;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItem;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

/**
 * Plugin implementation of the 'datedeferfield' field type.
 *
 * @FieldType(
 *   id = "datedeferfield",
 *   label = @Translation("Date defer field"),
 *   description = @Translation("Green / Yellow / Red dates with custom logic."),
 *   default_widget = "datedeferfield_default",
 *   default_formatter = "datedeferfield_default",
 *   list_class = "\Drupal\datedeferfield\Plugin\Field\FieldType\DateDeferFieldItemList",
 *   constraints = {"DateDeferDateFormat" = {}}
 * )
 *
 * @todo Add constraint.
 */
class DateDeferFieldItem extends DateTimeItem {

  public static function components() {
    return [
      'green' => t('Green'),
      'yellow' => t('Yellow'),
      'red' => t('Red'),
    ];
  }

  /**
   * {@inheritDoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = [];
    foreach (static::components() as $key => $label) {
      $properties["value_$key"] = DataDefinition::create('datetime_iso8601')
        ->setLabel(t('@label value', ['@label' => $label]))
        ->setRequired(FALSE);

      $properties["date_$key"] = DataDefinition::create('any')
        ->setLabel(t('@label date', ['@label' => $label]))
        ->setDescription(t('The computed @label DateTime object.', ['@label' => $label]))
        ->setComputed(TRUE)
        ->setClass(DateComputed::class)
        ->setSetting('date source', "value_$key");
    }

    return $properties;
  }

  /**
   * {@inheritDoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $parentSchema = parent::schema($field_definition);
    $schema = [];
    foreach (static::components() as $key => $label) {
      $schema['columns']["value_$key"] = [
          'description' => t('The @label value', ['@label' => $label])
        ] + $parentSchema['columns']['value'];
      $schema['indexes']["value_$key"] = ["value_$key"];
    }
    return $schema;
  }

  /**
   * {@inheritDoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public static function defaultStorageSettings() {
    return [];
  }

  /**
   * {@inheritDoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $values = [];
    $requestTime = \Drupal::time()->getRequestTime();
    foreach (static::components() as $key => $label) {
      // Just pick a date in the past year.
      $timestamp = $requestTime - mt_rand(0, 86400 * 365) - 86400;
      $values["value_$key"] = gmdate(DateTimeItemInterface::DATE_STORAGE_FORMAT, $timestamp);
    }
    return $values;
  }

  /**
   * {@inheritDoc}
   */
  public function isEmpty() {
    foreach (static::components() as $key => $label) {
      $value = $this->get("value_$key")->getValue();
      $isEmpty = $value === NULL || $value === '';
      if (!$isEmpty) {
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function onChange($property_name, $notify = TRUE) {
    if (preg_match('/^value_(.*)$]/', $property_name, $matches)) {
      $key = $matches[1];
      $this->{"date_$key"} = NULL;
    }
    parent::onChange($property_name, $notify);
  }

}
