CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

An experimental module that provides a deferral field.
This module provides a green/yellow/red 3-date field with some logic.

More info:

 * For a full description of the module, visit [the project page]
   (https://www.drupal.org/project/datedeferfield).

REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install the Dete Defer Field module as you would normally install a 
   contributed Drupal module. Visit https://www.drupal.org/node/1897420 
   for further information.

CONFIGURATION
-------------

There is no configuration.


MAINTAINERS
-----------

Current maintainers:

 * Merlin Axel Rutz (geek-merlin) - https://www.drupal.org/u/geek-merlin
