<?php

/**
 * @file
 * Date defer field views data.
 */

use Drupal\field\FieldStorageConfigInterface;

/**
 * Implements hook_field_views_data_alter().
 */
function datedeferfield_field_views_data_alter(array &$data, FieldStorageConfigInterface $field_storage) {
  if ($field_storage->getType() === 'datedeferfield') {
    $table = "{$field_storage->getTargetEntityTypeId()}__{$field_storage->getName()}";
    if (isset($data[$table])) {
      $tData =& $data[$table];
      $fieldName = $field_storage->getName();
      // @todo Also care for revisions.
      // We only need green, as it is the only component that needs special
      // handling, such that null values are far past. This way a "green<now"
      // filter does the right thing and also selects green=null.
      // For yellow and red, null means never-will-be, so the standard 3-values
      // sql handling does the right thing here.
      $component = 'green';
      $old = "{$fieldName}_value_{$component}";
      $new = "{$fieldName}_value_{$component}_adjusted";

      $tData[$old]['filter']['id'] = 'datetime';

      $tData[$new]['group'] = $tData[$old]['group'];
      $tData[$new]['help'] = $tData[$old]['help'];
      $tData[$new]['title'] = $tData[$old]['title'] . ' (adjusted)';
      $tData[$new]['title short'] = $tData[$old]['title short'] . ' (adjusted)';
      $tData[$new]['filter'] = $tData[$old]['filter'];
      $tData[$new]['filter']['id'] = 'datedeferfield_adjusted';
    }
  }
}
